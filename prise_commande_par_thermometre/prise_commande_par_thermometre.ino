/* Dépendance pour le bus 1-Wire
 * 1-Wire = sonde de température
 */
#include <OneWire.h>

/* Broche du bus 1-Wire */
const byte BROCHE_ONEWIRE = 7;

/* Codes de retour de la fonction getTemperature() */
enum DS18B20_RCODES {
  READ_OK,  // Lecture ok
  NO_SENSOR_FOUND,  // Pas de capteur
  INVALID_ADDRESS,  // Adresse reçue invalide
  INVALID_SENSOR  // Capteur invalide (pas un DS18B20)
};

/* Création de l'objet OneWire pour manipuler le bus 1-Wire */
OneWire ds(BROCHE_ONEWIRE);

/* Dépendance pour l'afficheur */
#include <TM1637.h>

/* Pins for TM1637 */
#define CLK 9       
#define DIO 8

/* Création de l'objet OneWire pour manipuler l'afficheur */
TM1637 tm1637(CLK,DIO);
/* Broche du relais */
#define REL 6

/* Analog in du potentiomètre */
#define POT 0
/* Variable pour la valeur du potentiomètre */
int potval = 0;
boolean haschanged = false;
String oldStatus = "x";

void setup() {
  /* Initialisation du port série */
  Serial.begin(115200);
  /* Communication avec le relais */
  pinMode(REL,OUTPUT);
 
  float t;
  getTemperature(&t, true);

  /* Initialisation de l'afficheur */
  tm1637.init();
  tm1637.set(BRIGHT_TYPICAL);
  /*BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7; */
  tm1637.display(0,INDEX_NEGATIVE_SIGN);
  tm1637.display(1,INDEX_NEGATIVE_SIGN); 
  tm1637.display(2,INDEX_NEGATIVE_SIGN);
  tm1637.display(3,INDEX_NEGATIVE_SIGN); // put degree
  delay(1000);
  
  /* Affichage de la valeur du potentiomètre */
  potval = getPot();
  int digitoneT = ((int) potval) / 10;
  int digittwoT = ((int) potval) % 10;
  tm1637.display(0,INDEX_BLANK);
  tm1637.display(1,digitoneT); 
  tm1637.display(2,digittwoT);
  tm1637.display(3,18); // put degree
  changeRelay(t);

  delay(2000);
  tm1637.point(POINT_OFF);
}

void loop() {
  int newpot = getPot();

  if (newpot == potval) {
    Serial.print(F("newpot: "));
    Serial.println(newpot);
    Serial.print(F("potval: "));
    Serial.println(potval);
    if (haschanged) {
      Serial.println(F("Potientometer has changed. Waiting 1 seconds"));
      potval = newpot;
      delay(250);
      haschanged = false;
    }
    newpot = getPot();
    if (newpot == potval) {
      float t = printTemp();
      changeRelay(t);
    } else {
      potval = newpot;
      printPotVal();
      haschanged = true;
    }
  } else {
    potval = newpot;
    printPotVal();
    haschanged = true;
  }
}

int getPot() {
  float val = (float) analogRead(POT);
  Serial.print("Valeur du potentiometre : ");
  Serial.println(val);
  int pot = (int) (val / 1023 * 99);
  Serial.print("Valeur du potentiometre corrigee : ");
  Serial.println(pot);
  return pot;
}

void printPotVal() {
  int digitoneT = ((int) potval) / 10;
  int digittwoT = ((int) potval) % 10;
  tm1637.display(0,INDEX_BLANK);
  tm1637.display(1,digitoneT); 
  tm1637.display(2,digittwoT);
  tm1637.display(3,18); // put degree
}

float printTemp() {
  float temperature;
  /* Lit la température ambiante à ~1Hz */
  if (getTemperature(&temperature, true) != READ_OK) {
    Serial.println(F("Erreur de lecture du capteur"));
    if (getTemperature(&temperature, true) == NO_SENSOR_FOUND) {
      Serial.println(F("NO_SENSOR_FOUND"));
    } else if (getTemperature(&temperature, true) == INVALID_ADDRESS) {
      Serial.println(F("INVALID_ADDRESS"));
    } else if (getTemperature(&temperature, true) == INVALID_SENSOR) {
      Serial.println(F("INVALID_SENSOR"));
    }
    return 0;
  }

  /* Affiche la température dans le port série */
  Serial.print(F("Temperature : "));
  Serial.print(temperature, 2);
  Serial.write(176); // Caractère degré
  Serial.write('C');
  Serial.println();
  
  /* Affiche la température sur l'afficheur */
  int digitoneT = ((int) temperature) / 10;
  int digittwoT = ((int) temperature) % 10;
  tm1637.display(0,digitoneT); 
  tm1637.display(1,digittwoT);
  tm1637.display(2,18); // put degree
  tm1637.display(3,12); // put a C at the end
  
  return temperature;
}

void changeRelay(float temperature) {
  if (temperature < potval) {
    Serial.println(F("Open relay"));
    digitalWrite(REL,LOW);
    if (oldStatus.equals("HIGH")) {
      delay(250);
    }
    oldStatus = "LOW";
  } else {
    Serial.println(F("Close relay"));
    digitalWrite(REL,HIGH);
    if (oldStatus.equals("LOW")) {
      delay(250);
    }
    oldStatus = "HIGH";
  }
}
/**
 * Fonction de lecture de la température via un capteur DS18B20.
 */
byte getTemperature(float *temperature, byte reset_search) {
  byte data[9], addr[8];
  // data[] : Données lues depuis le scratchpad
  // addr[] : Adresse du module 1-Wire détecté
  
  /* Reset le bus 1-Wire ci nécessaire (requis pour la lecture du premier capteur) */
  if (reset_search) {
    ds.reset_search();
  }
 
  /* Recherche le prochain capteur 1-Wire disponible */
  if (!ds.search(addr)) {
    // Pas de capteur
    return NO_SENSOR_FOUND;
  }
  
  /* Vérifie que l'adresse a été correctement reçue */
  if (OneWire::crc8(addr, 7) != addr[7]) {
    // Adresse invalide
    return INVALID_ADDRESS;
  }
 
  /* Vérifie qu'il s'agit bien d'un DS18B20 */
  if (addr[0] != 0x28) {
    // Mauvais type de capteur
    return INVALID_SENSOR;
  }
 
  /* Reset le bus 1-Wire et sélectionne le capteur */
  ds.reset();
  ds.select(addr);
  
  /* Lance une prise de mesure de température et attend la fin de la mesure */
  ds.write(0x44, 1);
  delay(800);
  
  /* Reset le bus 1-Wire, sélectionne le capteur et envoie une demande de lecture du scratchpad */
  ds.reset();
  ds.select(addr);
  ds.write(0xBE);
 
 /* Lecture du scratchpad */
  for (byte i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
   
  /* Calcul de la température en degré Celsius */
  *temperature = ((data[1] << 8) | data[0]) * 0.0625; 
  
  // Pas d'erreur
  return READ_OK;
}
