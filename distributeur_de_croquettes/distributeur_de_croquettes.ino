#include <TimeAlarms.h>
#include <DS1302RTC.h>
#include <TimeLib.h>
#include <Time.h>
#include <Servo.h>

/* Arduino Uno
 *  
 * Branchement DS1302 :
 *  gnd sur gnd (noir)
 *  vcc sur 3.5v (rouge)
 *  rst sur le digital 5 (blanc)
 *  sda sur le digital 6 (marron)
 *  scl sur le digital 7(jaune)
 *  
 * Branchement servo-moteur :
 *  marron sur gnd
 *  rouge sur 5v
 *  orange sur le digital 13
 */

uint8_t RST_PIN = 5; //RST pin attach to
uint8_t SDA_PIN = 6; //IO pin attach to
uint8_t SCL_PIN = 7; //clk Pin attach to
DS1302RTC rtc(RST_PIN, SDA_PIN, SCL_PIN);

Servo myservo;  // create servo object to control a servo

void setup()
{
  /* Une seule fois : mise à l'heure du DS1302 */
  /*setTime(21, 5, 0, 12, 11, 2017);
  rtc.set(now());*/
  /* Initialisation du lien serie */
  Serial.begin(9600);

  /* Verification de l'horloge */
  if (rtc.haltRTC())
    Serial.print("Clock stopped!");
  else
    Serial.print("Clock working.");
  
  /* Synchronisation de l'horloge de l'arduino avec le DS1302 */
  setSyncProvider(rtc.get);
  /* Verification de la synchronisation */
  if (timeStatus() == timeSet)
    Serial.print(" Ok!\n");
  else
    Serial.print(" FAIL!\n");

  print_time();
        
  /* Initialisation du servo */
  myservo.attach(13);
  delay(3000);
  myservo.write(170);
  myservo.detach();
  Serial.print("Servo initialized\n");
  /* Mise en place des alarmes */
  Alarm.alarmRepeat(7, 0, 0, Alarma);
  Alarm.alarmRepeat(19, 0, 0, Alarma);
  /* Juste pour les tests */
  /*Alarm.timerRepeat(30, Alarma);*/
}

void loop()
{
  Alarm.delay(1000);
}

void Alarma()
{
  Serial.print("Alarma!\n");
  for (int i=0; i < 4; i++){
    deliver();
    delay(1000);
    shake();
  }
  for (int i=0; i < 2; i++){
    shake();
  }
}

void deliver()
{
  print_time();
  myservo.attach(13);
  Serial.print("Delivering\n");
  myservo.write(1);
  delay(3000);
  myservo.write(90);
  Serial.print("Delivered\n");
  myservo.detach();
}

void shake()
{
  Serial.print("Shaking\n");
  myservo.attach(13);

  /* On secoue un peu */
  myservo.write(180);
  delay(500);
  myservo.write(90);
  delay(500);
  myservo.write(180);
  delay(500);
  myservo.write(90);
  delay(500);
  myservo.write(180);
  delay(500);
  myservo.write(90);
  delay(500);
  myservo.write(180);
  delay(500);
  myservo.write(90);
  
  myservo.detach();
  Serial.print("Shaked\n");
}

void print_time()
{
  Serial.print(day());
  Serial.print("-");
  Serial.print(month());
  Serial.print("-");
  Serial.print(year());
  Serial.print(" ");
  Serial.print(hour());
  Serial.print(":");
  Serial.print(minute());
  Serial.print(":");
  Serial.println(second());
}
